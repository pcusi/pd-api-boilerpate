# Node.js | Express | Typescript | TDD

Hi everyone, I want to share this boilerplate API for all that not want to configure a project with TDD from zero.

If you like this project, please press the "Star" button and fork it!. Sorry for miss "the l" in boilerplate word.

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/pcusi/pd-api-boilerpate.git
```

Go to the project directory

```bash
  cd pd-api-boilerpate
```

Install dependencies

```bash
  npm install
  npm run tsc:interface
```

Start the server

```bash
  npm run dev
```

## Running Tests

To run tests, run the following command. You can see your coverage at the folder called "coverage", you should use the index.html
and press your navigation explorer preference.

```bash
  npm run test:coverage
```

## Contributing

Contributions are always welcome!

See `contributing.md` for ways to get started.

Please adhere to this project's `code of conduct`.

## Author
- [@pcusir](https://gitlab.com/pcusi)

## Support

For support, email pcusir@gmail.com.

## Badges
[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)
[![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)