"use strict";

module.exports = {
  "check-coverage": true,
  "report-dir": "./coverage",
  reporter: ["html", "text"],
  branches: 100,
  functions: 100,
  lines: 100,
  statements: 100,
};
