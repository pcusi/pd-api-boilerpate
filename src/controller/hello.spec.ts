import { app } from "@server/index";
import request from "supertest";

describe("Hello Service", () => {
  it("should say hello in hello router", (done) => {
    request(app).get("/api/v1/hello").expect(200, done);
  });

  it("should say ok ok in bad_role message", (done) => {
    request(app)
      .get("/api/v1/capitalize")
      .expect({
        message: `Ok can't change something in this ok module, please log with an admin user`,
      })
      .expect(200, done);
  });

  it("should say hello in init route", (done) => {
    request(app).get("/").expect({ message: "Hello!" }).expect(200, done);
  });
});
