import { bad_role } from "@infrastructure/constants";
import { Request, Response } from "express";

export const hello = (_req: Request, res: Response) => {
  return res.status(200).json({ message: "hello world" });
};

export const capitalize = (_req: Request, res: Response) => {
  return res.status(200).json({ message: bad_role("ok", "ok") });
};
