import { capitalizeFirstLetter } from "@utils/capitalize";
import { _logger } from "@utils/logger";
import * as dotenv from "dotenv";

dotenv.config();

export const server = (port: number | string): void =>
  _logger.info(`Server is listening on port ${port}`);

export const bad_role = (role: string, module: string) =>
  `${capitalizeFirstLetter(
    role,
  )} can't change something in this ${module} module, please log with an admin user`;
