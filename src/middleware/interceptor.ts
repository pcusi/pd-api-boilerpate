import { HttpStatusCode, Messages } from "@infrastructure/enums";
import * as dotenv from "dotenv";
import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { get } from "lodash";

dotenv.config();

export const interceptor = (req: Request, res: Response, next: NextFunction) => {
  const headers: string | string[] | undefined =
    req.headers.authorization || req.headers["Authorization"];

  if (!headers)
    return res
      .status(HttpStatusCode.Unauthorized)
      .json({ message: Messages.UNAUTHORIZED });

  const token: string = get(req, "headers.authorization", "");

  verify(token, `${process.env.PD_SECRET_KEY}`, (error, user) => {
    if (error)
      return res
        .status(HttpStatusCode.Forbidden)
        .json({ message: Messages.NO_PERMISSION });
    req.sub = get(user, "sub", 0);
    next();
  });
};
