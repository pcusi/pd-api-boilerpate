import { capitalize, hello } from "@controller/hello.controller";
import { Router } from "express";

const router: Router = Router();

router.get("/hello", hello);
router.get("/capitalize", capitalize);

export { router };
