/* Routes */
import { server } from "@infrastructure/constants";
import * as AppRouter from "@routes/app.routes";
import cors from "cors";
import * as dotenv from "dotenv";
import express from "express";

dotenv.config();

export const app = express();
const port = process.env.PORT || 4000;

/* Middlewares */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  cors({
    origin: true,
    credentials: true,
  }),
);

const routes: express.Router[] = [AppRouter.router];

app.use("/api/v1", [...routes]);

app.get("/", (_req: express.Request, res: express.Response) => {
  return res.json({ message: "Hello!" });
});

app.listen(port, () => {
  server(port);
});

declare global {
  namespace Express {
    interface Request {
      sub: number | string | (() => string);
    }
  }
}
